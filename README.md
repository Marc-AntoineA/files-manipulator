# Files manipulator

This repository contains a set of useful Python package to manipulate various files: conversion, compression, etc.

It uses intensively command line Linux utilities.

* [images-manipulator](/images-manipulator/README.md) to work with images
* [fonts-manipulator](/fonts-manipulator/README.md) to convert fonts in various formats
