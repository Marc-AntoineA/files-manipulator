

## Description

_Why this Merge Request?_


**Issues** :

## Checklist

- [ ] A test had been created
- [ ] All the tests are Ok. No broken tests.
- [ ] Lint is ok.
- [ ] If a new requirement is required, add it in [setup.py](/images-manipulator/setup.py)
- [ ] Update the version in [setup.py](/images-manipulator/setup.py).
    * Update `PATCH` if it's a bug fix ;
    * Update `MINOR` if it's a new feature ;
    * Update `MAJOR` if broken api or a major feature
- [ ] Add an entry to the [changelog](/images-manipulator/changelog.md)
- [ ] No remaining comments or print