
# Fonts Manipulator

## Features

### Convert fonts formats

**Reference** : https://dev.to/jankapunkt/converting-my-otf-font-into-multiple-web-fonts-with-this-bash-script-m1l

## Requirements

You need a Linux system.

```
apt-get install -y eot2ttf mkeot woff-tools woff2 fontforge
```

## Getting started

```
python3 cli.py
```
