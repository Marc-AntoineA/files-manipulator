import subprocess
import os

def _exec(command):
    subprocess.call(command, shell=True)

def convert(input_file, output_extension):

    input_extension = os.path.splitext(input_file)[1]

    output_file = os.path.splitext(input_file)[0] + output_extension

    if os.path.exists(output_file):
        print(f'{ output_file } already exists. Skip')
        return output_file

    if input_extension == '.eot' and output_extension == '.ttf':
        command = f'eot2ttf "{ input_file }" "{ output_file }"'
        _exec(command)

    if input_extension == '.eot' and output_extension == '.otf':
        command = f'eot2otf "{ input_file } "{ output_file }"'
        _exec(command)

    elif input_extension == '.otf' and output_extension == '.ttf':
        output_file_eot = convert(input_file=input_file, output_extension='.eot')
        output_file = convert(input_file=output_file_eot, output_extension='.ttf')

    elif input_extension == '.otf' and output_extension == '.eot':
        command = f'mkeot "{ input_file }" > "{ output_file }"'
        _exec(command)

    elif input_extension == '.ttf' and output_extension == '.eot':
        command = f'mkeot "{ input_file }" > "{ output_file }"'
        _exec(command)

    elif input_extension == '.ttf' and output_extension == '.otf':
        command = f"fontforge -lang=ff -c 'Open($1); Generate($2); Close();' '{ input_file }' '{ output_file }'"
        _exec(command)

    elif input_extension == '.ttf' and output_extension == '.woff':
        output_file_otf = convert(input_file=input_file, output_extension='.otf')
        output_file = convert(input_file=output_file_otf, output_extension='.woff')

    elif input_extension == '.otf' and output_extension == '.woff':
        command = f'sfnt2woff "{ input_file }" > "{ output_file }"'
        _exec(command)

    elif input_extension == '.woff' and output_extension == '.woff2':
        command = f'woff2_compress "{ input_file }" > "{ output_file }"'
        _exec(command)

    elif output_extension == '.woff2':
        output_file_woff = convert(input_file=input_file, output_extension='.woff')
        output_file = convert(input_file=output_file_woff, output_extension='.woff2')
        command = f'woff2_compress { input_file } > { output_file }'
        _exec(command)

    elif input_extension == '.woff' and output_extension in ('.otf', '.ttf'):
        command = f"fontforge -lang=ff -c 'Open($1); Generate($2); Close();' { input_file } { output_file }"
        _exec(command)

    else:
        print(f'Unabled to convert { input_extension } --> { output_extension }')

    print(f'Convert { input_file } --> { output_extension }')

    return output_file

if __name__ == '__main__':
    import argparse


    # eot --> ttf

    parser = argparse.ArgumentParser(description='Convert fonts between formats')
    parser.add_argument('--input', '-i', type=str, required=True,
                        help='Path to the file to convert')
    parser.add_argument('--otf', action='store_true',
                        help='Add this option to convert to otf')
    parser.add_argument('--ttf', action='store_true',
                        help='Add this option to convert to ttf')
    parser.add_argument('--woff', action='store_true',
                        help='Add this option to convert to woff')
    parser.add_argument('--woff2', action='store_true',
                        help='Add this option to convert to woff2')

    args = parser.parse_args()
    if args.otf:
        convert(input_file= args.input, output_extension='.otf')
    if args.ttf:
        convert(input_file= args.input, output_extension='.ttf')
    if args.woff:
        convert(input_file= args.input, output_extension='.woff')
    if args.woff2:
        convert(input_file= args.input, output_extension='.woff2')


