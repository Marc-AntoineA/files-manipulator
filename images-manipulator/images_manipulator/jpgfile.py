from typing import Optional

from .abstract_file import AbstractFile


class JpgFile(AbstractFile):

    def __init__(self, filepath: Optional[str] = None, suffix: Optional[str] = None):
        super().__init__(filepath, suffix)

    @property
    def extensions(self):
        return ['.jpeg', '.jpg', '.JPG', '.JPEG']

    @property
    def suffix(self):
        return '.jpg'

    @staticmethod
    def mimetype() -> str:
        return 'image/jpeg'

    def _compress(self, verbose=False):  # -> JpgFile:
        '''Internal function for compress'''
        result = JpgFile(None)
        self.copy(result.filepath, force=True)
        self.execute_command(['jpegoptim', result.filepath, '--max', '75'])
        return result

    def resize(self, width, height):
        '''
        Resize image. Inplace
        '''
        self.assert_exists()
        command = f'convert { self.filepath } -resize { width }x{ height } { self.filepath }'
        self.execute_command(command)
        return self

    def to_webp(self, width, height): # -> WebpFile
        '''
        Resize image width x height
        '''

        from .webpfile import WebpFile

        out_file = WebpFile()
        command = f'convert { self.filepath} -density 72x72 -resize { width }x{ height } { out_file.filepath }'
        self.execute_command(command)
        return out_file

    def to_png(self): # -> PngFile
        '''
        Resize image width x height
        '''

        from .pngfile import PngFile

        out_file = PngFile()
        self.execute_command(['convert', self.filepath, out_file.filepath])
        return out_file
