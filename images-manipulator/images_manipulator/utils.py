
import base64

def from_base64(base64_string): # -> File
    '''
    Build and return a temporary file for the given base64_string
    '''
    from .pngfile import PngFile
    image = None
    if base64_string.startswith(f'data:{ PngFile.mimetype() };base64,'):
        image = PngFile()
        base64_string = base64_string[len(
            f'data:{ PngFile.mimetype() };base64,'):]
    else:
        raise NotImplementedError()

    with open(image.filepath, 'wb') as f:
        f.write(base64.b64decode(base64_string))

    return image
