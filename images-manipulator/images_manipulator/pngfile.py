from typing import Optional, List, Tuple

from .abstract_file import AbstractFile
from .pnmfile import PnmFile

import cv2
import copy
import numpy as np
from sklearn.cluster import KMeans
from sklearn.utils import shuffle
from matplotlib import colors
from PIL import Image

class PngFile(AbstractFile):

    def __init__(self, filepath: Optional[str]=None, suffix: Optional[str]=None):
        super().__init__(filepath, suffix)

    @property
    def extensions(self):
        return ['.png']

    @property
    def suffix(self):
        return '.png'

    @staticmethod
    def mimetype() -> str:
        return 'image/png'

    def has_alpha_channel(self):
        '''Return true iff the image has alpha channel'''
        img = cv2.imread(self.filepath, cv2.IMREAD_UNCHANGED)
        if len(img.shape) == 2:  # gray image
            return False
        _, _, d = tuple(img.shape)
        if d == 3:  # rgb image (no alpha channel)
            return False
        return True

    @property
    def width(self):
        try:
            return float(self.execute_command(['identify', '-format', '%w', self.filepath]))
        except Exception as e:
            if self.size() == 0:
                return 0
            raise e

    @property
    def height(self):
        try:
            return float(self.execute_command(['identify', '-format', '%h', self.filepath]))
        except Exception as e:
            if self.size() == 0:
                return 0
            raise e


    def _compress(self, verbose=False): # -> PngFile:
        '''Internal function for compress'''
        result = PngFile(None)
        result.unlink()
        command = ['pngquant', self.filepath, '--output', result.filepath]
        self.execute_command(command)
        return result

    def to_svg(self, is_black_and_white: bool=False, nb_colors: int=1, white_is_alpha: bool=False, canonize: bool=False): # -> SvgFile
        if is_black_and_white:
            assert nb_colors == 1 # Does not make sense otherwise
            pnm_image = self.to_pnm()
            svg_image = pnm_image.to_svg(canonize)
            return svg_image
        else:
            _, masks = self.posterize(nb_colors, white_is_alpha=white_is_alpha)
            svgs = []
            for mask, color in masks:
                svgs.append(mask.to_svg(is_black_and_white=True, canonize=False))
                svgs[-1].change_color({"#000000": color})

            svgs[0].extend(svgs[1:])

            svgs[0].canonize()
            return svgs[0]

    def to_pnm(self): # -> PnmFile
        result = PnmFile(None)
        command = f'convert -alpha Remove "{ self.filepath }" "{ result.filepath }"'
        self.execute_command(command)
        return result

    def posterize(self, nb_colors: int, white_is_alpha=True)\
        -> Tuple[any, List[Tuple]]: # -> Tuple[PngFile, List[Tuple[PngFile, str]]]
        '''
        Create nb_colors temporary images, one in each color.
        Based on opencv and sklearn
        '''

        def _bgr_to_rgb(bgr):
            bgr = bgr / 255.
            return [max(0, bgr[2]), max(0, bgr[1]), max(0, bgr[0])] # r, g, b


        result_files = []

        img = cv2.imread(self.filepath, cv2.IMREAD_UNCHANGED)
        if len(img.shape) == 2:  # gray image
            img = cv2.merge([img, img, img])

        w, h, d = tuple(img.shape)
        if d == 3:  # rgb image (no alpha channel)
            img = cv2.cvtColor(img, cv2.COLOR_RGB2RGBA)

        if white_is_alpha:
            # if  (img[i, j, 0] > 250) and  (img[i, j, 2] > 250) and  (img[i, j, 3] > 250) or then img[i, j, 4] = 0
            img[:, :, 3] = 255*(1 - (img[:, :, 0] > 250) * (img[:, :, 1] > 250) * (img[:, :, 2] > 250)) * (1 - (img[:, :, 3] == 0))


        w, h, d = tuple(img.shape)
        assert d == 4  # rgba

        image_array = np.reshape(img, (w * h, d))
        image_array_no_transparency = image_array[np.where(image_array[:, 3] > 0)[0], :3]

        image_array_sample = shuffle(image_array_no_transparency, random_state=0, n_samples=min(10_000, image_array_no_transparency.shape[0]))
        kmeans = KMeans(n_clusters=nb_colors, random_state=0).fit(image_array_sample)

        labels = kmeans.predict(image_array[:, :3])
        output_img = copy.copy(img)
        output_img[:, :, :3] = kmeans.cluster_centers_[labels].reshape(w, h, -1)

        quantized_image = PngFile()
        cv2.imwrite(quantized_image.filepath, output_img)

        for label in range(nb_colors):
            colored_img = np.zeros((w, h, 4))

            good_pixels = (labels == label).reshape(w, h, -1) * (img[:, :, 3] > 0).reshape(w, h, 1)
            colored_img[:, :, 0:1] = 0*good_pixels
            colored_img[:, :, 1:2] = 0*good_pixels
            colored_img[:, :, 2:3] = 0*good_pixels
            colored_img[:, :, 3:4] = 255*good_pixels

            bgr = kmeans.cluster_centers_[label]
            html = colors.rgb2hex(_bgr_to_rgb(bgr))

            result_files.append((PngFile(), html))
            cv2.imwrite(result_files[-1][0].filepath, colored_img)

        return quantized_image, result_files

    def compute_hitregion(self): # -> PngFile
        '''
        Compute a new image with hitregion
        '''

        img = Image.open(self.filepath).convert('L')
        array = np.array(img)

        def _dist(black1, black2):
            return max(abs(black1[0] - black2[0]), abs(black1[1] - black2[1]))

        def _is_black_surrounded(i, j, array):
            if i > 0 and array[i - 1, j] > 0:
                return False
            if j > 0 and array[i, j - 1] > 0:
                return False
            if i < array.shape[0] - 1 and array[i + 1, j] > 0:
                return False
            if j < array.shape[1] - 1 and array[i, j + 1] > 0:
                return False
            return True

        def _compute_segment(i1, j1, i2, j2, T):
            segment = set()
            T = float(T)
            for t in range(int(T)):
                i = t/T*i1 + (T - t)/T*i2
                j = t/T*j1 + (T - t)/T*j2
                segment.add((round(i), round(j)))
            return segment

        blacks = []

        new_blacks = set()
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                if array[i, j] != 255:
                    new_blacks.add((i, j))
                    array[i, j] = 0

        itr = 0

        while len(new_blacks) > 100 and itr < 8:
            blacks.extend(new_blacks)

            copy_blacks = []
            for (i, j) in blacks:
                if _is_black_surrounded(i, j, array):
                    continue
                copy_blacks.append((i, j))
            blacks = copy_blacks

            new_blacks = set()
            for i in range(len(blacks)):
                black1 = blacks[i]
                for j in range(i + 1, len(blacks)):
                    black2 = blacks[j]
                    if _dist(black1, black2) >= 8:
                        continue
                    segment = _compute_segment(
                        black1[0], black1[1], black2[0], black2[1], 10)
                    for (ii, jj) in segment:
                        if array[ii, jj] == 0:
                            continue
                        array[ii, jj] = 0
                        new_blacks.add((ii, jj))
            itr += 1

        result_image = PngFile()
        img.save(result_image.filepath)
        return result_image

    def scale(self, scale):
        '''
        Scale image. Inplace
        '''
        command = f'convert { self.filepath } -resize { round(100*scale, 3) }% { self.filepath }'
        self.execute_command(command)
        return self

    def resize(self, width, height, force=False):
        '''
        Scale image. Inplace.
        If force=True, output image will be distorted to perfectly fit
        '''
        self.assert_exists()
        command = ['convert', self.filepath, '-resize', f'{int(width)}x{int(height)}' + ('!' if force else ''), self.filepath]
        self.execute_command(command)
        return self

    def to_webp(self, width, height):
        '''
        Resize image width x height
        '''

        from .webpfile import WebpFile

        out_file = WebpFile()
        command = f'convert { self.filepath} -density 72x72 -resize { width }x{ height } { out_file.filepath }'
        self.execute_command(command)
        return out_file


    def compute_hull(self): # -> PngFile
        '''
        Compute the connexe hull of the image.
        Full image if no alpha channel
        '''
        img = cv2.imread(self.filepath, cv2.IMREAD_UNCHANGED)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        stack = []
        w, h, = img.shape

        visited = 255*np.ones((w, h))

        def _append(i, j, stack, img, w, h, visited):
            if i < 0 or j < 0 or i >= w or j >= h:
                return

            if visited[i, j] == 0:
                return

            if img[i, j] < 254:
                return

            visited[i, j] = 0
            stack.append((i, j))

        def _append_neighbors(i, j, stack, img, w, h, visited):
            _append(i-1, j, stack, img, w, h, visited)
            _append(i+1, j, stack, img, w, h, visited)
            _append(i-1, j-1, stack, img, w, h, visited)
            _append(i+1, j-1, stack, img, w, h, visited)
            _append(i-1, j+1, stack, img, w, h, visited)
            _append(i+1, j+1, stack, img, w, h, visited)

        def _append_bounds(i, ii, j, jj, stack, img, w, h, visited):
            for ix in range(i, ii):

                _append(ix, j, stack, img, w, h, visited)
                _append(ix, jj-1, stack, img, w, h, visited)

            for jx in range(j, jj):
                _append(i, jx, stack, img, w, h, visited)
                _append(ii-1, jx, stack, img, w, h, visited)

        _append_bounds(0, w, 0, h, stack, img, w, h, visited)
        img = np.array(img)

        k = 0
        while len(stack) > 0:
            k += 1
            top = stack[0]
            stack.pop(0)
            (i, j) = top
            _append_neighbors(i, j, stack, img, w, h, visited)

        visited = 255*np.ones((w, h)) - visited
        hull_image = PngFile()
        cv2.imwrite(hull_image.filepath, visited)

        return hull_image

    def compute_similarity(self, other, **kwargs):
        '''
        Compute the structural similarity index with the other image.
        Using skimage.metrics.

        **kwargs is skimage.metrics.structural_similarity kwargs
        '''
        from skimage.metrics import structural_similarity as ssim
        s = ssim(cv2.imread(self.filepath, cv2.IMREAD_GRAYSCALE),
                 cv2.imread(other.filepath, cv2.IMREAD_GRAYSCALE),
                 **kwargs)
        return s

    def to_jpg(self):  # -> JpgFile:
        '''
        Convert to jpg file
        '''
        from .jpgfile import JpgFile
        result = JpgFile()
        self.execute_command(['convert', self.filepath, result.filepath])
        result.assert_exists()
        return result
