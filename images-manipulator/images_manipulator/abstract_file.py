from typing import Optional, List
import subprocess
import tempfile
import datetime
import shutil
import os


class AbstractFile:

    _filepath: str
    _is_temporary_file: bool

    def __init__(self, filepath: Optional[str]=None, suffix: Optional[str]=None):

        if filepath is not None:
            self._filepath = filepath
            # Shoul be initialized before calling check_filepath
            self._is_temporary_file = False
            self.check_filepath()
        else:
            self._filepath = tempfile.NamedTemporaryFile(
                delete=False, suffix=suffix if suffix is not None else self.suffix).name
            self._is_temporary_file = True

    def __lt__(self, other):
        return self._filepath < other._filepath

    def __del__(self):
        if self._is_temporary_file:
            self.unlink()

    def __str__(self):
        return self._filepath

    def unlink(self):
        os.unlink(self.filepath)

    def copy(self, output_path: Optional[str]=None, force=False):

        if output_path is None:
            res = type(self)(output_path)
            shutil.copy(self.filepath, res.filepath)
            return res

        if os.path.exists(output_path) and not force:
            raise ValueError(f' { output_path } already exists')

        shutil.copy(self.filepath, output_path)
        return type(self)(output_path)

    def move(self, output_path, force=False):
        if os.path.exists(output_path) and not force:
            raise ValueError(f' { output_path } already exists')
        shutil.move(self.filepath, output_path)
        self._is_temporary_file = False
        self._filepath = output_path
        return self

    @property
    def filepath(self) -> str:
        return self._filepath

    @property
    def extension(self) -> str:
        return os.path.splitext(self._filepath)[1]

    @property
    def suffix(self) -> str:
        raise NotImplementedError('Suffix should be overriden')

    @property
    def extensions(self) -> List[str]:
        raise NotImplementedError('extensions should be overriden')

    @staticmethod
    def mimetype() -> str:
        raise NotImplementedError('mimetype should be overriden')

    def check_filepath(self):
        '''Check if fillename points to a svg/svgz filename'''
        if not os.path.exists(self.filepath):
            raise FileNotFoundError(f'File "{ self.filepath }" does not exist')
        for extension in self.extensions:
            if self.filepath.endswith(extension):
                break
        else:
            raise NotImplementedError(
                f'{ str(type(self)) } requires extensions in { self.extensions } files (not "{ self.filepath }").')

    def execute_command(self, command: str | List, verbose=False):
        '''Execute a command'''

        # TODO handle errors
        if verbose:
            print(f'> Executing: { command }')
        if isinstance(command, list):
            p = subprocess.run(command, stdout=subprocess.PIPE, check=True, stderr=subprocess.PIPE)
        else:
            p = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)

        return p.stdout.decode()

    def change_date(self, date: str):
        '''Update the date of path'''
        command = f'touch -d "{ date }" "{ self.filepath }"'
        self.execute_command(command)

    def modification_date(self) -> datetime.datetime:
        self.check_filepath()
        timestamp = os.path.getmtime(self.filepath)
        return datetime.datetime.fromtimestamp(timestamp)

    def size(self) -> int:
        '''Return the size of the file (in bytes)'''
        return os.path.getsize(self.filepath)

    def assert_exists(self):
        assert os.path.exists(self.filepath)\
            and self.size() > 0

    def compress(self, output_filepath: Optional[str]=None, verbose=True, **args): #-> AbstractFile
        '''
        Compress the file.
        Keep the same timestamp

        Compress inplace if output_filepath is None
        '''
        self.assert_exists()

        inplace = output_filepath is None

        initial_size = self.size()
        modification_date = self.modification_date()
        compressed_file = self._compress(verbose=verbose, **args)
        if inplace:
            compressed_file = compressed_file.move(self.filepath, force=True)
            compressed_file.change_date(str(modification_date))
            compressed_file = self

        compressed_size = compressed_file.size()
        if verbose:
            ratio = round(100 - float(compressed_size)/initial_size * 100, 1)
            print(f'> Compressed { initial_size } bytes --> { compressed_size } bytes (-{ ratio }%)')
        return compressed_file

    def set_permanent(self):
        '''
        Do not remove file on delete
        '''
        self._is_temporary_file = False