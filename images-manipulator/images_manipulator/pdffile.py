from bs4 import BeautifulSoup
from typing import Optional, Dict, List

from .abstract_file import AbstractFile

class PdfFile(AbstractFile):

    def __init__(self, filepath: Optional[str]=None, suffix: Optional[str]=None):
        super().__init__(filepath, suffix)

    @property
    def extensions(self):
        return ['.pdf']

    @property
    def suffix(self):
        return '.pdf'


    def to_svg(self, font_strategy='keep'): # SvgFile
        '''
        Convert the pdf file to svg using Inkscape
        '''
        from .svgfile import SvgFile

        out_file = SvgFile()

        self.execute_command(['inkscape',
                              self.filepath, 
                              '--export-filename', out_file.filepath,
                              '-n', '1', # pdf page
                              '--pdf-font-strategy', font_strategy
                              ])
        # out_file.remove_mono_pixel_masks()
        return out_file

    def to_png(self): # PngFile
        '''
        Convert the pdf file to png using ghostscript.
        '''
        from .pngfile import PngFile
        out_file = PngFile()
        # https://ghostscript.com/docs/9.54.0/Use.htm
        self.execute_command(['gs', '-dSAFER', '-dBATCH', '-dNOPAUSE', '-sDEVICE=png16m', f'-sOutputFile={ out_file.filepath }', self.filepath])
        return out_file

    def to_jpg(self): # JpgFile
        '''
        Convert the pdf file to png using Magick.
        '''
        from .jpgfile import JpgFile
        out_file = JpgFile()
        self.execute_command(['convert', '-density', '150', self.filepath, '-quality', '100', out_file.filepath])
        return out_file

    def _compress(self, verbose=True, resize=False):
        '''
        Compress all images and convert them as jpeg or png
        depending on the compressed weight.
        Very similar to SvgFile::compress_binary_images

        Images with transparency remain as png.
        Images with masks are ignored.
        Inspired by:
        https://medium.com/@pymupdf/how-to-manipulate-images-in-a-pdf-using-python-23ec153d7ce6

        :replace -> to resize all images to the pdf boundingbox

        '''
        import fitz
        from .pngfile import PngFile
        from .jpgfile import JpgFile

        doc = fitz.open(self.filepath)

        queue = []
        set_images = set()

        if verbose:
            print('Step 1/ Extracting images')

        for page in doc:
            images = page.get_images()
            print(f'> page { page } ({ len(images) } image(s) found)')
            for image in images:
                xref = image[0]
                if xref in set_images:
                    continue
                set_images.add(xref)
                smask = image[1]
                image_content = doc.extract_image(xref)
                if image_content['ext'] == 'png':
                    image_file = PngFile()
                elif image_content['ext'] == 'jpeg':
                    image_file = JpgFile()

                out = open(image_file.filepath, "wb")
                out.write(image_content["image"])  # write the binary content
                out.close()
                visible_rect = page.get_image_rects(xref)
                if len(visible_rect) != 1:
                    continue
                visible_rect = visible_rect[0]
                queue.append((xref, visible_rect, image_file, smask))

        if verbose: print(f'Step 2/ Compressing { len(queue) } images')
        for (xref, visible_rect, image_file, smask) in queue:
            if smask > 0:
                continue
            print('will compress', image_file.filepath)

            # visible rect is in pt
            # 1  px = 72/96 pt
            print('resize', resize)
            if resize:
                image_file.resize(width=visible_rect.width*96./72, height=visible_rect.height*96./72)
            compressed_image = image_file.compress()
            if image_file.mimetype() == 'image/png' and not image_file.has_alpha_channel():
                jpeg_image = image_file.to_jpg()
                jpeg_image.compress()
                if jpeg_image.size() < compressed_image.size():
                    compressed_image = jpeg_image

            elif image_file.mimetype() == 'image/jpeg':
                png_image = image_file.to_png()
                png_image.compress()
                if png_image.size() < compressed_image.size():
                    compressed_image = png_image

            new_xref = page.insert_image(
                page.rect, filename=compressed_image.filepath
            )
            doc.xref_copy(new_xref, xref)  # copy over new to old
            last_contents_xref = page.get_contents()[-1]
            # new image insertion has created a new /Contents source,
            # which we will set to spaces now
            doc.update_stream(last_contents_xref, b" ")

        result = PdfFile()
        doc.save(result.filepath, garbage=4)  # save changed PDF
        return result