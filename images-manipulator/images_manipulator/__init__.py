from .svgfile import SvgFile
from .pngfile import PngFile
from .jpgfile import JpgFile
from .mp4file import Mp4File
from .pdffile import PdfFile