import re
import os
import subprocess
from bs4 import BeautifulSoup
from typing import Optional, Dict, List

from .abstract_file import AbstractFile

class SvgFile(AbstractFile):

    def __init__(self, filepath: Optional[str]=None, suffix: Optional[str]=None):
        super().__init__(filepath, suffix)
        self.check_inkscape_installation()

    @property
    def extensions(self):
        return ['.svg', '.svgz']

    @property
    def suffix(self):
        return '.svg'

    @property
    def width(self):
        command_get_width = f'inkscape "{ self.filepath }" -W'
        width = float(self.execute_command(command_get_width))
        return width

    @property
    def height(self):
        command_get_height = f'inkscape "{ self.filepath }" -H'
        height = float(self.execute_command(command_get_height))
        return height

    def check_inkscape_installation(self):
        # TODO check version and avaibilitys
        pass

    def compress_scour(self, suffix: Optional[str]=None):
        '''
        Convert svg using scour. Return a new file.
        '''
        options = ['--shorten-ids', '--indent=none', '--disable-simplify-colors', '--enable-comment-stripping', '--remove-descriptions',
                   '--remove-metadata', '--remove-descriptive-elements', '--no-line-breaks', '--create-groups', '--set-precision', '4']
        compressed_svg = SvgFile(suffix=suffix)
        command = ['scour', self.filepath, compressed_svg.filepath] + options
        subprocess.check_call(command)
        compressed_svg.assert_exists()
        return compressed_svg

    def export_element_area(self, element_id, only_element=False): # PngFile
        '''
        Export the element_id area
        '''
        from .pngfile import PngFile

        png_file = PngFile()
        command = ['inkscape', self.filepath, f'--export-id={ element_id }', '-o', png_file.filepath]
        if only_element:
            command.append('--export-id-only')
        self.execute_command(command)
        return png_file

    def replace_binary_images_by_svg(self, svg_files: List): #-> SvgFile
        '''
        Very similair (complementary) to 'compress_binary_images'.

        Look at all binary_images in svg and see if some are very similar to the ones as input files.
        Replace them by their svg version

        Used to replace logos for example.

        Inplace
        '''
        from .pngfile import PngFile

        soup = self.soup
        images = list(soup.find_all('svg:image'))
        for image in images:
            original_image = self.export_element_area(image['id'], only_element=True)

            if not isinstance(original_image, PngFile):
                raise NotImplementedError('??')

            w = original_image.width
            if w < 10: # Should be grower than 7 for similarities
                continue
            h = original_image.height

            for png_model, svg_file, similarity_threshold in svg_files:

                original_image.resize(png_model.width, png_model.height, force=True)
                similarity = png_model.compute_similarity(original_image)

                if similarity > similarity_threshold:
                    svg_file = svg_file.copy()

                    image_width = float(image['width'])
                    image_height = float(image['height'])

                    scale_x = image_width / svg_file.width
                    scale_y = image_height / svg_file.height
                    svg_file.apply_transforms()
                    svg_file.scale(scale_x, scale_y)
                    svg_file.apply_transforms()
                    svg_soup = svg_file.soup

                    new_image = svg_soup.new_tag('g',
                                                id=image['id'],
                                                width=image['width'], height=image['height'],
                                                transform=image['transform'])
                    new_image.append(svg_soup.find('svg:g'))
                    soup.find('svg:image', {"id": image['id']}).replaceWith(new_image)


                    self.set_svg_string(str(soup))
                    break

    def compress_binary_images(self): # -> SvgFile
        '''
        Takes all binary images from the svg. Resize all to just fit and compress using
        appropriated algorithms (pngquant and jpegoptim).
        Inplace
        '''
        from .pngfile import PngFile
        from .utils import from_base64
        import base64

        soup = self.soup

        global_dimensions = (self.width, self.height)
        images = list(soup.find_all('svg:image'))
        for image in images:

            original_image = from_base64(image['xlink:href'])
            visible_area: PngFile = self.export_element_area(image['id'])
            visible_area_width = visible_area.width

            # not visible image means that the image is not on the card (BAD)
            # Or it's a mask (GOOD).
            not_a_mask = visible_area_width > 1

            # We don't know where mask are used
            if not_a_mask:
                if 'clip-path' not in image.attrs:
                    original_image.resize(visible_area_width, visible_area.height)
                else:
                    # Does not work if image is croped and zoomed but :/
                    original_image.resize(global_dimensions[0], global_dimensions[1])

            compressed_image = original_image.compress()
            if not_a_mask and not original_image.has_alpha_channel():
                jpeg_image = original_image.to_jpg()
                jpeg_image.compress()
                if jpeg_image.size() < compressed_image.size():
                    compressed_image = jpeg_image

            with open(compressed_image.filepath, 'rb') as f:
                new_base64_image = base64.encodebytes(f.read()).decode('utf8')

                soup.find('svg:image', {"id": image['id']})['xlink:href'] = \
                    f'data:{ compressed_image.mimetype() };base64,{ new_base64_image}'

        self.set_svg_string(str(soup))
        return self


    def fit_to_drawing(self): # -> SvgFile
        out_file = SvgFile()
        command_fit_to_drawing = f'inkscape --batch-process --actions="file-open:{ self.filepath };select-all;fit-canvas-to-selection;export-filename:{ out_file.filepath };export-do;file-close"'
        self.execute_command(command_fit_to_drawing)
        return out_file

    @property
    def svg_string(self):
        if self.extension == '.svgz':
            self.compress_scour(suffix='.svg').copy(self.filepath)
            raise NotImplementedError
        with open(self.filepath) as f:
            return f.read()

    @property
    def soup(self) -> BeautifulSoup:
        return BeautifulSoup(self.svg_string, 'xml')

    def set_svg_string(self, svg_string):
        if self.extension == '.svgz':
            raise NotImplementedError
        with open(self.filepath, 'w') as f:
            f.write(svg_string)

    def extend(self, svgs):

        merged_soup = self.soup

        for svg in svgs:
            soup = svg.soup
            svg_tag = soup.find('svg')
            for child in svg_tag.findChildren(recursive=False):
                merged_soup.find('svg').append(child)

        self.set_svg_string(str(merged_soup))
        self.fit_to_drawing()

    def change_color(self, colors: Dict[str, str]):
        svg_string = self.svg_string

        for original_color in colors.keys():
            svg_string = re.sub(original_color, 'CC' +
                                original_color[1:], svg_string)

        for original_color, new_color in colors.items():
            svg_string = re.sub('CC' + original_color[1:], new_color, svg_string)
        self.set_svg_string(svg_string)

    def extract_colors(self) -> List[str]:
        text = self.svg_string

        regex = r'#([0-9a-fA-F]{6})'

        text = re.sub(r'pagecolor="#([a-f]|\d)*"', '', text)
        text = re.sub(r'bordercolor="#([a-f]|\d)*"', '', text)

        result = re.findall(regex, text)
        colors = list(set(result))

        return colors

    def format_colors(self):
        text = self.svg_string

        regex = r'#([0-9a-fA-F]{6})'

        text = re.sub(r'pagecolor="#([0-9a-fA-F])*"', '', text)
        text = re.sub(r'bordercolor="#([0-9a-fA-F])*"', '', text)

        # Todo handle all 3 chars colors
        text = re.sub(r'#fff;', '#ffffff', text)
        result = re.findall(regex, text)
        colors = list(set(result))
        for color in colors:
            lowerCaseColor = color.lower()
            if color == lowerCaseColor:
                continue
            text = re.sub(color, lowerCaseColor, text)

        self.set_svg_string(text)

    def to_png(self, expected_width: Optional[float] = None, expected_height: Optional[float] = None,
                    minimum_size: Optional[float] = None, export_background:str='white'):

        from .pngfile import PngFile

        if expected_width is not None and expected_height is not None:
            raise ValueError(f'Bad args. Please provid expected_with or expected_height')
        if expected_width is None and expected_height is None and minimum_size is None:
            raise ValueError(f'Bad args. Please provid expected_with or expected_height')

        image_width = self.width
        image_height = self.height

        ratio = float(image_height) / image_width

        if expected_width is not None:
            new_width = expected_width
            new_height = new_width * ratio
        elif expected_height is not None:
            new_height = expected_height
            new_width = new_height / ratio
        elif image_width <= image_height:
            new_width = minimum_size
            new_height = new_width * ratio
        else:
            new_height = minimum_size
            new_width = new_height / ratio

        new_height = int(round(new_height))
        new_width = int(round(new_width))

        out_file = PngFile()
        command = ['inkscape', '-w', str(new_width), self.filepath, '-o', out_file.filepath]
        if export_background is not None:
            command.append(f'--export-background={export_background}')
        self.execute_command(command)
        return out_file

    def canonize(self):
        self.replace_default_fill_with_black()
        self.format_colors()
        res = self.fit_to_drawing()
        res = res.compress_scour()
        os.rename(res.filepath, self.filepath)
        self.change_unit_to_px()

    def change_unit_to_px(self):
        '''
        Remove all occurences of 'pt' and 'mm' in the svg file
        '''
        svg_string = re.sub('pt', '', self.svg_string)
        svg_string = re.sub('mm', '', svg_string)
        self.set_svg_string(svg_string)

    def convert_all_colors_in_black(self): # SvgFile
        '''
        Create a temporary file where all hex colors are replaced with #000000 (black)
        '''
        svg_string_black = re.sub(r'#([0-9a-fA-F])*', '#000000', self.svg_string)
        black_svg = SvgFile()
        black_svg.set_svg_string(svg_string_black)
        return black_svg

    def replace_default_fill_with_black(self):
        '''
        Add fill: #000000 when fill is undefined
        Inplace
        '''
        soup = self.soup
        svg  = soup.find('svg')
        svg.attrs['fill'] = '#000000'
        self.set_svg_string(str(soup))
        return

    def apply_transforms(self):
        '''
        Apply all 'transforms' inplace
        Inspired by https://github.com/Klowner/inkscape-applytransforms
        '''
        from inkex.transforms import Transform
        from inkex.paths import Path

        IDENTITY_MATRIX = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]

        def _recursively_apply_transform_svg(node, previous_transform_matrix=IDENTITY_MATRIX):
            node_transform_attr = node.get('transform')
            if node_transform_attr:
                del node.attrs['transform']

            total_transform = Transform(
                previous_transform_matrix) * Transform(node_transform_attr)
            if total_transform != Transform(IDENTITY_MATRIX):
                if node.name == 'g':
                    pass
                elif node.name != 'path':
                    print(f'Error. Unsupported tag { node.name }')
                    return
                else:
                    path_str = node.get('d')
                    path = Path(path_str).to_absolute(
                    ).transform(total_transform, True)
                    node.attrs['d'] = str(path)

            for child in node.findChildren(recursive=False):
                _recursively_apply_transform_svg(child, total_transform)

        soup = self.soup
        _recursively_apply_transform_svg(soup.find('svg'))
        self.set_svg_string(str(soup))

    def scale(self, scale_x, scale_y=None):
        '''
        Apply uniform scale factor in svg (inplace).
        If scale_y is None, use uniform scale (scale_y = scale_x)
        '''
        if scale_y is None: scale_y = scale_x

        soup = self.soup

        for x in soup.find_all('path'):
            x['transform'] = 'matrix({},0,0,{},0,0)'.format(str(scale_x), str(scale_y))

        self.set_svg_string(str(soup))

    def extract_path(self) -> str:
        '''
        Concat all the paths into one and return its value
        '''
        soup = self.soup

        paths = list(soup.find_all('svg:path')) + list(soup.find_all('path'))

        merged_path = ''
        for x in paths:
            str_path = x['d']
            str_path = 'M' + str_path[1:]
            merged_path += str_path
        return merged_path

    def compute_hitpath(self, debug=False) -> str:
        '''
        Compute the region where the svg can be selected.
        The region is given with a given svg
        '''
        black_svg = self.convert_all_colors_in_black()
        black_png = black_svg.to_png(minimum_size=100)
        black_png_hitregion = black_png.compute_hitregion()
        black_hitregion_svg = black_png_hitregion.to_svg(is_black_and_white=True, canonize=True)

        scale_factor = self.width / black_hitregion_svg.width
        black_hitregion_svg.apply_transforms()
        black_hitregion_svg.scale(scale_factor)
        black_hitregion_svg.apply_transforms()

        if debug:
            black_svg.set_permanent()
            black_png_hitregion.set_permanent()
            black_hitregion_svg.set_permanent()
        return black_hitregion_svg.extract_path()

    def compute_hull(self, debug=False) -> str:
        '''
        '''

        old_width = self.width
        png_file = self.to_png(expected_width=min(400, self.width))
        new_width = png_file.width

        png_hull = png_file.compute_hull()
        svg_hull = png_hull.to_svg(is_black_and_white=True)

        scale_factor = old_width/new_width
        svg_hull.apply_transforms()
        svg_hull.scale(scale_factor)
        svg_hull.apply_transforms()

        return svg_hull.extract_path()

    def prefixes_all_ids(self, prefix):
        '''
        Replace all ids with prefix_id inplace
        '''
        str_svg = self.svg_string
        str_svg = str_svg.replace('svg:', '')

        regex_id = r'id="([^"]*)"'
        subst_id = r'id="{}_\1"'.format(prefix)
        str_svg = re.sub(regex_id, subst_id, str_svg)

        regex_id = r'"url\(#([^"]+)\)"'
        subst_id = r'"url(#{}_\1)"'.format(prefix)
        str_svg = re.sub(regex_id, subst_id, str_svg)
        self.set_svg_string(str_svg)

    def remove_mono_pixel_masks(self):
        '''
        Delete the masks with x=0, y=0, width=1, height=1
        Inplace
        '''
        soup = self.soup
        masks = list(soup.find_all('svg:mask'))
        for mask in masks:
            if mask.attrs['x'] != '0':
                continue
            if mask.attrs['y'] != '0':
                continue
            if mask.attrs['width'] != '1':
                continue
            if mask.attrs['height'] != '1':
                continue
            mask.extract()
        self.set_svg_string(str(soup))