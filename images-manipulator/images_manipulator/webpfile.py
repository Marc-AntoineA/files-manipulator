from typing import Optional, Dict, List
from .abstract_file import AbstractFile

class WebpFile(AbstractFile):

    def __init__(self, filepath: Optional[str] = None, suffix: Optional[str] = None):
        super().__init__(filepath, suffix)

    @property
    def extensions(self):
        return ['.webp']

    @property
    def suffix(self):
        return '.webp'

    def _compress(self, verbose=False):  # -> Webpfile:
        '''Internal function for compress'''

        result = WebpFile(None)
        result.unlink()
        command = ['cwebp', '-q', '100', self.filepath, '-o', self.filepath]
        self.execute_command(command)
        return result
