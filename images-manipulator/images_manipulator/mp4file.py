from typing import Optional

from .abstract_file import AbstractFile


class Mp4File(AbstractFile):

    def __init__(self, filepath: Optional[str] = None, suffix: Optional[str] = None):
        super().__init__(filepath, suffix)

    @property
    def extensions(self):
        return ['.mp4', '.MP4']

    @property
    def suffix(self):
        return '.mp4'

    def resize(self, verbose=True):
        initial_size = self.size()
        modification_date = self.modification_date()

        result = Mp4File(None)
        result.unlink()

        command = f'''ffmpeg -i "{ self.filepath }" -vcodec libx265 -crf 28 - filter: v "scale='min(480,iw)':min'(360,ih)':force_original_aspect_ratio=decrease,pad=480:360:(ow-iw)/2:(oh-ih)/2" "{ result.filepath }"'''
        self.execute_command(command)

        compressed_size = result.size()
        if verbose:
            ratio = round(100 - float(compressed_size)/initial_size * 100, 1)
            print(f'> Compressed { initial_size } bytes --> { compressed_size } bytes (-{ ratio }%)')

        return result

    def _compress(self, verbose=False):  # -> Mp4File:
        '''Internal function for compress'''
        result = Mp4File(None)
        result.unlink()

        command = f'ffmpeg -i "{ self.filepath }" -c:v libx265 "{ result.filepath }"'
        self.execute_command(command)
        return result

