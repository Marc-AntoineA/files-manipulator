from typing import Optional

from .abstract_file import AbstractFile
from .svgfile import SvgFile

class PnmFile(AbstractFile):

    def __init__(self, filepath: Optional[str]=None, suffix: Optional[str]=None):
        super().__init__(filepath, suffix)

    @property
    def extensions(self):
        return ['.pnm']

    @property
    def suffix(self):
        return '.pnm'

    def to_svg(self, canonize=True): # -> SvgFile
        ''''
        Vectorize monochrome png image using potrace
        '''
        result = SvgFile(None)
        command = f'potrace "{ self.filepath }" --svg -o "{ result.filepath }"'
        self.execute_command(command)
        if canonize:
            result.canonize()
        return result
