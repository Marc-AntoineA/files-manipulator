import setuptools

requirements = [
    'opencv-python',
    'scikit-learn',  # it's sklearn
    'matplotlib',
    'inkex',
    'scikit-image', # it's skimage,
    'PyMuPDF', # fitz
]

setuptools.setup(
    name="images_manipulator",
    version="0.0.10",
    author="Marc-AntoineA",
    description="Set of functions to manipulate images",
    license="MIT",
    install_requires=requirements,
    packages=['images_manipulator'],
    zip_safe=False,
)
