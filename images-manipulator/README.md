# Manipulator

The Python library to manipulate and compress all kind of images.

## Use Cases

### Aktivisda
_See code [on Framagit](https://framagit.org/aktivisda/aktivisda/-/blob/main/server/main.py)_

* Vector images: `png`/`jpeg` --> `svg`
* Compute previews : `svg` --> `png` or `jpeg`
* Detect colors and posterize `png` / `jpeg`
* Resize images and compute size

### Memo
_See code [on Framagit](https://framagit.org/memo-fresques/memo-viewer/-/tree/main/scripts/utils)_

* Convert `pdf` pages to `svg` and compress everything

## Installation

```
git clone <>
pip install images_manipulator
```

## Usage

```py
from images_manipulator import ...
```

## Dependencies

* `identify` to get `width` and `height` of png files
* `pngquant` for `png` compression
* `inkscape` to manipulate vectors
* `potrace`
* `magick` (and `convert`)

## Development

### Tests

```
pip install -U pytest
```

Just call `pytest`

You can specify the python file:
```
pytest test_pdffile.py
```

Or a specify Python test
```
pytest test_pdffile.py::test_pdffile_to_svg4
```