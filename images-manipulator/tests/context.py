import os
import sys

DIRNAME = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.join(DIRNAME, '..', 'images_manipulator'))

import images_manipulator