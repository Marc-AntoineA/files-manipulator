import os
from .context import images_manipulator as im

DIRNAME = os.path.dirname(os.path.realpath(__file__))

def test_move():
    '''
    Just move a png file and check that it does not raise any warning
    '''
    # Given
    image = im.PngFile(os.path.join(
        DIRNAME, 'assets', 'elephant-wikimedia.png')).copy()
    if os.path.exists('temporaryimage.png'):
        os.unlink('temporaryimage.png')

    # When
    image.move('temporaryimage.png')

    # Then
    image.assert_exists()
    assert os.path.exists('temporaryimage.png')
    os.unlink('temporaryimage.png')

def test_compress_and_move():
    '''
    Just move a png file and check that it does not raise any warning
    '''
    # Given
    image = im.PngFile(os.path.join(
        DIRNAME, 'assets', 'elephant-wikimedia.png')).copy()

    if os.path.exists('temporaryimage.png'):
        os.unlink('temporaryimage.png')
    
    # When
    image.compress().move('temporaryimage.png')

    # Then
    assert os.path.exists('temporaryimage.png')
    image.assert_exists()
    os.unlink('temporaryimage.png')