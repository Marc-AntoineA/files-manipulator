from .context import images_manipulator as im
import os
DIRNAME = os.path.dirname(os.path.realpath(__file__))


def test_identical_images():

    # Given
    image1 = im.PngFile(os.path.join(DIRNAME, 'assets', 'elephant-wikimedia.png'))
    image2 = im.PngFile(os.path.join(DIRNAME, 'assets', 'elephant-wikimedia.png'))

    # When
    similarity_index = image1.compute_similarity(image2)

    # Then
    assert similarity_index == 1.0