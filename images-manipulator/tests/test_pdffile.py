import os
import pytest
from bs4 import BeautifulSoup

from .context import images_manipulator as im

DIRNAME = os.path.dirname(os.path.realpath(__file__))

def test_pdffile_to_svg1():
    '''
    Convert pdf_tosvg1.pdf into svg and compare the png previews
    of both svg and pdf using PngFile.computeSimilarity

    This test should fail if you don't have Roboto Black installed on your computer.
    This font is used by Inkscape to generate the right svg file.
    '''
    # Given
    pdf_file = im.PdfFile(os.path.join(DIRNAME, 'assets/pdf_tosvg1.pdf'))

    # When
    svg_file = pdf_file.to_svg()

    # Then
    pdf_file_thumbnail = pdf_file.to_png().resize(width=300, height=300)
    svg_file_thumbnail = svg_file.to_png(expected_width=300).resize(width=300, height=300)

    assert pdf_file_thumbnail.compute_similarity(svg_file_thumbnail, win_size=21) >= 0.8


def test_pdffile_to_svg2():
    '''
    Convert pdf_to_svg2.pdf into svg and compare the png previews.
    (same as test_pdffile_to_svg1)

    This is a non-regression test for bug #4: pdf --> svg is sometimes broken
    (fixed In Inkscape 1.3 (1:1.3+202307231459+0e150ed6c4))
    '''

    # Given
    pdf_file = im.PdfFile(os.path.join(DIRNAME, 'assets/pdf_tosvg2.pdf'))

    # When
    svg_file = pdf_file.to_svg()

    # Then
    pdf_file_thumbnail = pdf_file.to_png().resize(width=300, height=300)
    svg_file_thumbnail = svg_file.to_png(
        expected_width=300).resize(width=300, height=300)

    assert pdf_file_thumbnail.compute_similarity(
        svg_file_thumbnail, win_size=21) >= 0.8


def test_converted_svg_has_text():
    '''
    Convert pdf --> svg and check if the resulting svg still have
    text inside (I don't want texts converted to paths).

    Can happen if we use pdftocairo for conversion
    '''
    # Given
    pdf_file = im.PdfFile(os.path.join(DIRNAME, 'assets/pdf_tosvg2.pdf'))

    # When
    svg_file = pdf_file.to_svg().compress_binary_images()

    # Then
    soup = BeautifulSoup(svg_file.svg_string, 'xml')
    assert len(list(soup.find_all('text'))) > 0


@pytest.mark.skip(reason="This test requires a fix in Inkscape (issue #4545)")
def test_pdffile_to_svg3():
    '''
    Convert pdf_to_svg3.pdf into svg and compare the png previews.
    (same as test_pdffile_to_svg1)

    This test is due to a regression in Inkscape 1.3 (1:1.3+202307231459+0e150ed6c4)
    See issue #4545
    '''

    # Given
    pdf_file = im.PdfFile(os.path.join(DIRNAME, 'assets/pdf_tosvg3.pdf'))

    # When
    svg_file = pdf_file.to_svg()
    svg_file.copy('svg.svg', force=True)
    # Then
    pdf_file_thumbnail = pdf_file.to_png().resize(width=300, height=300)
    svg_file_thumbnail = svg_file.to_png(
        expected_width=300).resize(width=300, height=300)

    assert pdf_file_thumbnail.compute_similarity(
        svg_file_thumbnail, win_size=21) >= 0.9


def test_pdffile_to_svg4():
    '''
    Convert pdf_to_svg4.pdf into svg and compress.
    Bug: the compression failed due to mask presences (visible area == 0)

    Please note that the svg and the pdf differs due to a bug in Inkscape
    '''

    # Given
    pdf_file = im.PdfFile(os.path.join(DIRNAME, 'assets/pdf_tosvg4.pdf'))

    # When
    svg_file = pdf_file.to_svg()
    svg_file = pdf_file.to_svg().compress_binary_images().compress_scour()

    # Then
    svg_file.assert_exists()


def test_pdffile_to_svg5():
    '''
    Convert pdf_to_svg5.pdf into svg and compress.
    There are two problems with this pdf:
    * first: it exists small useless 1 x 1 masks.
     Removed using svsFile::remove_mono_pixel_masks
     (automaticaly called in to_svg)
    * second: many images (to not say all of them) are displayed using a mask.
      it means that the displayed area is much more smaller than the image
    '''

    # Given
    pdf_file = im.PdfFile(os.path.join(DIRNAME, 'assets/pdf_tosvg5.pdf'))

    # When
    svg_file = pdf_file.to_svg()
    svg_file = pdf_file.to_svg().compress_binary_images().compress_scour()

    # Then
    svg_file.assert_exists()
    svg_file.copy('svgfile.svg', force=True)

def test_pdffile_to_svg6():
    '''
    Convert pdf_to_svg6.pdf into svg and compress.
    The problem with this case is to keep the mask around the apple
    '''

    # Given
    pdf_file = im.PdfFile(os.path.join(DIRNAME, 'assets/pdf_tosvg6.pdf'))

    # When
    svg_file = pdf_file.to_svg()
    svg_file = pdf_file.to_svg().compress_binary_images().compress_scour()

    # Then
    svg_file.assert_exists()
    svg_file.copy('svgfile.svg', force=True)


def test_compression_no_resize():
    # Given
    pdf_file = im.PdfFile(os.path.join(DIRNAME, 'assets/pdf_compression.pdf')).copy()

    # When
    pdf_file = pdf_file.compress(resize=False)

    # Then
    assert pdf_file.size() <  210000

def test_compression_resize():
    # Given
    pdf_file = im.PdfFile(os.path.join(DIRNAME, 'assets/pdf_compression.pdf')).copy()

    # When
    pdf_file = pdf_file.compress(resize=True)

    # Then
    assert pdf_file.size() <  67000
