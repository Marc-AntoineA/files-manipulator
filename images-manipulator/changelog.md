# Changelog

## v0.0.10

**Images-manipulator** :
* feat: `SvgFile::canonize` define the default fill color as black so this color can be changed later in aktivisda. Add a test. (see issue [#8](https://framagit.org/Marc-AntoineA/files-manipulator/-/issues/8))


## v0.0.9

**Images-manipulator** :
* fix: `compute_hull` was broken for "small" svg. Add a test

## v0.0.8

**Images-manipulator**
* fix: compute_hitpath was broken due to a typo in scale method (see issue [#7](https://framagit.org/Marc-AntoineA/files-manipulator/-/issues/7))

## v0.0.7

**Images-manipulator**
* feat: pdf images compression using PyMuPDF (new dependency)
* test: new pdffile compression test

## v0.0.5

**Images-manipulator**
* New pdf --> svg test. The svg compression failed due to mask presences. PngFile::width was broken. Return 0 if size() == 0.
(please note that the resulted svg does not correspond to the pdf entry, this is a bug in Inkscape)

## v0.0.4
See MR !1

**Images-manipulator**
* Creating `pytest` architecture
* Working on pdf --> svg : tests and bugfixes ;
* Update to inkscape 1.3
* Improve SvgFile::BinaryImagesCompression