
import os

from images_manipulator import PngFile, JpgFile, Mp4File


def load_file(filepath, ignore_unknown):

    if filepath.endswith('.png'):
        f = PngFile(filepath)
    elif filepath.endswith('.jpeg')\
            or filepath.endswith('.jpg')\
            or filepath.endswith('.JPG')\
            or filepath.endswith('.JPEG'):
        f = JpgFile(filepath)
    elif filepath.endswith('.mp4')\
            or filepath.endswith('.MP4'):
        f = Mp4File(filepath)
    else:
        if ignore_unknown: return None
        raise NotImplementedError(f'Unsupported extension for { filepath }')

    return f

def compress_file(f):
    initial_size = f.size()
    compressed = f.compress()
    return initial_size, compressed.size()

def list_all_files(root_path):
    return [os.path.join(root, name)
        for root, _, files in os.walk(root_path)
        for name in files]

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Compress files')

    parser.add_argument('-i', '--input', type=str,
                        help='Path to the input file')

    args = parser.parse_args()

    if os.path.isdir(args.input):
        all_files = list_all_files(args.input)
        known_files = [load_file(f, ignore_unknown=True) for f in all_files if load_file(f, ignore_unknown=True) is not None]
        known_files.sort()

        print(f'Compress all files in { args.input }')
        print(f'> { len(all_files) } found | { len(known_files) } can be compressed')
        total_size = sum([f.size() for f in known_files])
        print(f'> Total size: { total_size }')
        initial_total_size = total_size

        for k, f in enumerate(known_files):
            # if type(f) != Mp4File:
            #     continue
            print(f'Compress : { f }')
            (b, a) = compress_file(f)
            total_size += a - b
            print(f'> Progress { k + 1 } / { len(known_files)} ({ round(100*(k + 1.) / len(known_files), 1) }%) { initial_total_size } bytes -->  { total_size } bytes (-{ round(100 - 100*float(total_size)/initial_total_size, 1) }%)')
    else:
        compress_file(load_file(args.input, ignore_unknown=False))

